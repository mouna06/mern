//var should = require('should')
const request = require("supertest")
var app = require('../routes/user.routes')


describe('Users', function (){
    describe('Test user', function () {
        it ('should get user', function(){
            request(app)
                .get('api/users/5ec52aeea6b06e136a91b1d1')
                .expect(200)
        })  ;
 
    it ('should create new user', function(){
        var user = {
            "name" : "CHAIBI",
            "contact" : "chaibi@testcool.com"
        }
        request(app)
            .post('/api/users')
            .send(user)
            .expect(201)
    })  ;

    it ('should update user', function(){
        var user = {
            "name" : "Mouna",
            "contact" : "chaibi@testcool.com"
        }
        request(app)
            .put('/api/users/5ec52aeea6b06e136a91b1d1')
            .send(user)
            .expect(200)
    })  ;
    it ('should delete user', function(){

        request(app)
            .delete('/api/users/5ec52aeea6b06e136a91b1d1')
            .expect(200)
    })  ;
});
})